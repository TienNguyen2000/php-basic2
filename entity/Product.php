<?php 
class Product{
    public $id;
    public $name;
    public $categoryId;
    public function construct($id, $name, $categoryId)
    {
       $this->id = $id;
       $this->name = $name;
       $this->categoryId = $categoryId;
    }
    public function getId(){
        return $this->id;
    }
    public function setId(){
        return $this->id;
    }
    public function getName(){
        return $this->name;
    }
    public function setName(){
        return $this->name;
    }
    public function getCategoryId(){
        return $this->categoryId;
    }
    public function setCategoryId(){
        return $this->categoryId;
    }
    
}